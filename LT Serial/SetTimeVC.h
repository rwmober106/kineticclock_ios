//
//  SetTimeVC.h
//  KinectClock2
//
//  Created by MacOS on 1/25/18.
//  Copyright © 2018 MacOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "ConnectVC.h"
#import "DarkTimeVC.h"

@class ConnectVC;

@interface SetTimeVC : BaseVC
@property (nonatomic, strong) ConnectVC *rootView;
@end
