//
//  SetTimeVC.m
//  KinectClock2
//
//  Created by MacOS on 1/25/18.
//  Copyright © 2018 MacOS. All rights reserved.
//

#import "SetTimeVC.h"

@interface SetTimeVC ()
@property (nonatomic, retain) IBOutlet UIDatePicker *myPickerDate;
@end

@implementation SetTimeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDate *myDate = [defaults objectForKey:@"SettingTime"];
    [_myPickerDate setDate:myDate];
}

- (IBAction)onSendString:(id)sender
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"a-hh:mm:ss"];
    NSString *sendString = [NSString stringWithFormat:@"TS:%@",[format stringFromDate:_myPickerDate.date]];
    
    [self.rootView onSendString:sendString];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_myPickerDate.date forKey:@"SettingTime"];
    [defaults synchronize];
}

- (IBAction)onForward:(id)sender
{
    DarkTimeVC *chooseVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DarkTimeVC"];
    chooseVC.rootView = self.rootView;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"a-hh:mm"];
    NSString *sendString = [format stringFromDate:_myPickerDate.date];
    
    chooseVC.strTime = sendString;
    [self.navigationController pushViewController:chooseVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
