//
//  BaseVC.m
//  KinectClock2
//
//  Created by MacOS on 1/26/18.
//  Copyright © 2018 MacOS. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [_lblLogoTitle setFont:[UIFont fontWithName:@"LaserStd" size:23]];
}

- (IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
