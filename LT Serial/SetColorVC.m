//
//  SetColorVC.m
//  KinectClock2
//
//  Created by MacOS on 1/25/18.
//  Copyright © 2018 MacOS. All rights reserved.
//

#import "SetColorVC.h"

@interface SetColorVC ()
@property (nonatomic, retain) IBOutlet UISlider *rSlider;
@property (nonatomic, retain) IBOutlet UISlider *gSlider;
@property (nonatomic, retain) IBOutlet UISlider *bSlider;
@property (nonatomic, retain) IBOutlet UISlider *wSlider;
@property (nonatomic, retain) IBOutlet UILabel  *lblValue;
@end

@implementation SetColorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _rSlider.value = [defaults integerForKey:@"Rvalue"];
    _gSlider.value = [defaults integerForKey:@"Gvalue"];
    _bSlider.value = [defaults integerForKey:@"Bvalue"];
    _wSlider.value = [defaults integerForKey:@"Wvalue"];
    
    _lblValue.text = [NSString stringWithFormat:@"R:%d G:%d B:%d W:%d", (int)_rSlider.value, (int)_gSlider.value, (int)_bSlider.value, (int)_wSlider.value];
    [_mySegument setSelectedSegmentIndex:[defaults integerForKey:@"SettingValue"]];
    _mySegument.frame = CGRectMake(_mySegument.frame.origin.x, _mySegument.frame.origin.y, _mySegument.frame.size.width, 30);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _wSlider.value = 100;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    _wSlider.value = 100;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onForward:(id)sender
{
    FilterVC *chooseVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterVC"];
    chooseVC.rootView = self.rootView;
    [self.navigationController pushViewController:chooseVC animated:YES];
}

- (IBAction)onSet:(id)sender
{
    int tag = (int)_mySegument.selectedSegmentIndex;
    int rValue = (int)_rSlider.value;
    int gValue = (int)_gSlider.value;
    int bValue = (int)_bSlider.value;
    int wValue = (int)_wSlider.value;
    NSString *sendString = @"";
    
    switch (tag) {
        case 0:
            sendString = [NSString stringWithFormat:@"TT:%d,%d,%d,%d", rValue , gValue, bValue, wValue];
            break;
        case 1:
            sendString = [NSString stringWithFormat:@"TF:%d,%d,%d,%d", rValue , gValue, bValue, wValue];
            break;
        case 2:
            sendString = @"SAVE";
            break;
        case 3:
            sendString = @"RESET";
            break;
    }
    
    [self.rootView onSendString:sendString];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:tag forKey:@"SettingValue"];
    [defaults setInteger:rValue forKey:@"Rvalue"];
    [defaults setInteger:gValue forKey:@"Gvalue"];
    [defaults setInteger:bValue forKey:@"Bvalue"];
    [defaults setInteger:wValue forKey:@"Wvalue"];
    [defaults synchronize];
}

- (IBAction)onUpdateValue:(id)sender
{
    int rValue = (int)_rSlider.value;
    int gValue = (int)_gSlider.value;
    int bValue = (int)_bSlider.value;
    int wValue = (int)_wSlider.value;
    _lblValue.text = [NSString stringWithFormat:@"R:%d G:%d B:%d W:%d", rValue, gValue, bValue, wValue];
}

@end
