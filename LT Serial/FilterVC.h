//
//  FilterVC.h
//  LT Serial
//
//  Created by MacOS on 2/1/18.
//  Copyright © 2018 Laird Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "ConnectVC.h"

@class ConnectVC;

@interface FilterVC : BaseVC
@property (nonatomic, strong) ConnectVC *rootView;
@end
