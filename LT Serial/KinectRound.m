//
//  KinectRound.m
//  LT Serial
//
//  Created by MacOS on 2/2/18.
//  Copyright © 2018 Laird Technologies. All rights reserved.
//

#import "KinectRound.h"

@implementation KinectRound

-(void)drawRect:(CGRect)rect
{
    self.layer.cornerRadius = 10;
    self.clipsToBounds = YES;
}

@end
