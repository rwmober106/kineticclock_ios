//
//  FilterVC.m
//  LT Serial
//
//  Created by MacOS on 2/1/18.
//  Copyright © 2018 Laird Technologies. All rights reserved.
//

#import "FilterVC.h"

@interface FilterVC ()

@end

@implementation FilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onRecall:(UIButton*)sender
{
    NSString *sendString = [NSString stringWithFormat:@"REC%ld:",(long)sender.tag];
    [self.rootView onSendString:sendString];
}

- (IBAction)onSave:(UIButton*)sender
{
    NSString *sendString = [NSString stringWithFormat:@"SAA%ld:",(long)sender.tag];
    [self.rootView onSendString:sendString];
}

@end
