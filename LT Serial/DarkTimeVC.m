//
//  DarkTimeVC.m
//  LT Serial
//
//  Created by MacOS on 3/7/18.
//  Copyright © 2018 Laird Technologies. All rights reserved.
//

#import "DarkTimeVC.h"

@interface DarkTimeVC ()
@end

@implementation DarkTimeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_strTime == nil) {
        _strTime = @"AM-06:00";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//AM-06:11

- (IBAction)onSet:(UIButton*)sender
{
    int tag = (int)sender.tag;
    NSString *sendString = @"";
    
    switch (tag) {
        case 1:
            sendString = [NSString stringWithFormat:@"DTSTART:%@", _strTime];
            break;
        case 2:
            sendString = [NSString stringWithFormat:@"DTSTOP:%@", _strTime];
            break;
        case 3:
            sendString = @"TMR1";
            break;
        case 4:
            sendString = @"TMR0";
            break;
        case 5:
            sendString = @"LON";
            break;
        case 6:
            sendString = @"LOF";
            break;
        case 7:
            sendString = @"DI1";
            break;
        case 8:
            sendString = @"DI0";
            break;
        case 9:
            sendString = @"DEM1";
            break;
        case 10:
            sendString = @"DEM0";
            break;
    }
    
    [self.rootView onSendString:sendString];
}

@end
