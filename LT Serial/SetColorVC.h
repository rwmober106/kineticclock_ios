//
//  SetColorVC.h
//  KinectClock2
//
//  Created by MacOS on 1/25/18.
//  Copyright © 2018 MacOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "ConnectVC.h"
#import "FilterVC.h"

@class ConnectVC;

@interface SetColorVC : BaseVC
@property (nonatomic, strong) ConnectVC *rootView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *mySegument;
@end
