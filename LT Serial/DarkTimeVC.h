//
//  DarkTimeVC.h
//  LT Serial
//
//  Created by MacOS on 3/7/18.
//  Copyright © 2018 Laird Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectVC.h"
#import "BaseVC.h"

@class ConnectVC;

@interface DarkTimeVC : BaseVC
@property (nonatomic, strong) ConnectVC *rootView;
@property (nonatomic, retain) NSString *strTime;
@end
