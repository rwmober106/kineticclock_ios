//
//  KinectButton.m
//  KinectClock2
//
//  Created by MacOS on 1/25/18.
//  Copyright © 2018 MacOS. All rights reserved.
//

#import "KinectButton.h"

@implementation KinectButton

-(void)drawRect:(CGRect)rect
{
    int thickness = 4;
    UIBezierPath* path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(self.bounds,thickness/2, thickness/2) cornerRadius: self.bounds.size.width/2];
    path.lineWidth = thickness;
    [[UIColor blackColor] setStroke];
    [path stroke];
}

@end
