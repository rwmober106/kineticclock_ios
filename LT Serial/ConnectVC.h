//
//  ViewController.h
//  nRF UART
//
//  Created by Ole Morten on 1/11/13.
//  Copyright (c) 2013 Nordic Semiconductor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "SetTimeVC.h"
#import "SetColorVC.h"
#import "DarkTimeVC.h"
#import "UARTPeripheral.h"

#define MAX_SCAN_DEVICES  10
#define MAX_SCAN_TIME     10
#define MIN_SCAN_TIME      5

@interface ConnectVC : BaseVC <UITextFieldDelegate, CBCentralManagerDelegate, UARTPeripheralDelegate>

@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UIButton *btnColor;
@property (weak, nonatomic) IBOutlet UIButton *btnTime;
@property (weak, nonatomic) IBOutlet UIButton *btnDisconnect;
@property (weak, nonatomic) IBOutlet UIButton *btnSF;

@property (nonatomic, strong) NSMutableArray  *devicePerpArray;
@property (nonatomic, strong) NSMutableArray  *deviceRssiArray;
@property unsigned int        rcvTimerCount;
@property unsigned int        scanComplete;
@property NSUInteger   isConnectButtonBusy;

- (IBAction)connectButtonPressed:(id)sender;
- (void) makeBtConnection:(NSInteger)index;
- (void) stopBtScan;
- (void)onSendString:(NSString*)sendString;

@end

