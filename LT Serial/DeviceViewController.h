//
//  DeviceViewController.h
//  LT Serial
//
//  Created by PINWU KAO on 6/2/14.
//  Copyright (c) 2014 Laird Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "ConnectVC.h"

@interface DeviceViewController : BaseVC
@property NSInteger selectedRow;

@property (nonatomic, strong) ConnectVC *rootView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)clearSelected:(id)sender;

@end
